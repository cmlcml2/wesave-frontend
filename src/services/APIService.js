import axios from 'axios'
const API_URL = 'https://jsonplaceholder.typicode.com'

export class APIService {
  getUsers() {
    const url = `${API_URL}/users`
    return axios.get(url).then(response => response.data)
  }

  getUser(id) {
    const url = `${API_URL}/users/${id}`
    return axios.get(url).then(response => response.data)
  }

  getArticles(id) {
    const url = `${API_URL}/posts?userId=${id}`
    return axios.get(url).then(response => response.data)
  }
}
